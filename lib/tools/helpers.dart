import './types.dart';

/// Return true or falls if Regex fails.
bool isUrl(String url) {
  return urlRegex.hasMatch(url);
}

/// Check if the request method matches ENUM
isValidMethod(String input) {
  input = input.toLowerCase();
  bool isValid = false;

  Method.values.forEach((v) => {
        if (v.toString().split('Method.')[1] == input) {isValid = true}
      });

  return isValid;
}
