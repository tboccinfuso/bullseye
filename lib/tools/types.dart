/// Method ENUM containing the valid request methods.
///
/// ```
/// await bullseye.customRequest(Method.get , {});
/// ```
enum Method { get, post, put, patch, delete, head, bytes, read }

/// Regex for matching a URL
final RegExp urlRegex = RegExp(
    '(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})');
