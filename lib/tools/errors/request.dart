const String AuthError =
    'It seems you are trying to make a request that requires Authentication but didn\'t provide and credentials. Check your Bullseye Config.';

const String MethodError = 'Invalid request method.';

const String BaseUrlMissingError =
    'Missing config["baseUrl"] from new Bullseye() call.';
