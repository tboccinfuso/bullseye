import './request/baseRequest.dart';
import './tools/helpers.dart';
import './tools/errors/bullseye.dart';

class Bullseye extends Request {
  // Constructor

  /// Creates a new [Bullseye] instance.
  ///
  /// Takes [Request.baseConfig] Mapping as it's param.
  Bullseye(Map<String, dynamic> baseConfig) : super(baseConfig);

  // Methods
  /// Attepmt to set [baseUrl] to passed in [String newBaseUrl].
  void set_new_baseUrl(String newBaseUrl) {
    if (isUrl(newBaseUrl)) {
      this.baseConfig['baseUrl'] = newBaseUrl;
    } else {
      throw new Exception(BaseUrlFail);
    }
  }

  void update_base_headers(Map<String, dynamic> headers) {
    this.baseConfig['headers'] = headers;
  }

  Map<dynamic, dynamic> get_base_config() {
    return this.baseConfig;
  }
}
