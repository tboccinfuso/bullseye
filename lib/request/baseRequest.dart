import 'dart:convert';
import 'package:http/http.dart' as http;
import '../tools/helpers.dart';
import '../tools/errors/request.dart';
import 'package:xml/xml.dart' as xml;

abstract class Request {
  Map<dynamic, dynamic> baseConfig = {
    'baseUrl': '',
    'requiresAuth': false,
    'headers': {
      'WWW-Authenticate': '',
      'Authorization': '',
      'Content-type': '',
      'Accept': ''
    }
  };

  // Constructors
  Request(Map<String, dynamic> baseConfig) {
    if (baseConfig['baseUrl'] == '' || baseConfig['baseUrl'] == null) {
      throw new Exception(BaseUrlMissingError);
    }

    this.baseConfig = baseConfig;
  }

  // Public Methods

  /// Make a GET request.
  ///
  /// [config] is optional, fallsback to baseConfig.
  Future<http.Response> get([Map<String, dynamic> config]) async {
    config = this._checkConfig(config);

    var isValidRequest = this._validateRequest(config, 'get');

    if (isValidRequest['isValid']) {
      try {
        return await http.get(config['baseUrl'], headers: config['headers']);
      } catch (e) {
        return new http.Response(
            'Invalid request to: ${config["baseUrl"]}', 400);
      }
    } else {
      return isValidRequest['response'];
    }
  }

  /// Make a POST request.
  ///
  /// [body] required, will format to match "Content-type" header
  ///
  /// [config] is optional, fallsback to baseConfig.
  Future<http.Response> post(dynamic body,
      [Map<String, dynamic> config]) async {
    config = this._checkConfig(config);

    var isValidRequest = this._validateRequest(config, 'post');

    if (isValidRequest['isValid']) {
      body = _formatBody(body, config['headers']);
      try {
        return await http.post(config['baseUrl'],
            headers: config['headers'], body: body);
      } catch (e) {
        return new http.Response(
            'Invalid request to: ${config["baseUrl"]}', 400);
      }
    } else {
      return isValidRequest['response'];
    }
  }

  /// Make a new custom request by allowing you to pass in overrides.
  ///
  /// [String] must match one of the [Method.<method>] types.
  ///
  /// [config] is a Map used to overwrite the base config [bullseye.base]
  Future<http.Response> customRequest(
      String method, Map<String, dynamic> config,
      [body]) async {
    config = this._checkConfig(config);
    var isValidRequest = this._validateRequest(config, method);

    if (isValidRequest['isValid']) {
      try {
        if (method.toLowerCase() == 'get') {
          return await http.get(config['baseUrl'], headers: config['headers']);
        } else if (method.toLowerCase() == 'post' && body) {
          return await http.post(config['baseUrl'],
              headers: config['headers'], body: body);
        }
      } catch (e) {
        return new http.Response(
            'Invalid request to: ${config["baseUrl"]}', 400);
      }
    } else {
      return isValidRequest['response'];
    }

    return http.Response('Internal Server Error :(', 500);
  }

  // Private Methods

  /// Ensures request is Valid by checking the following:
  ///
  /// Returns a 401 [http.Response] is not authorized.
  ///
  /// Returns a 400 [http.Response] if invalid [method].
  _validateRequest(config, String method) {
    if (this.baseConfig['requiresAuth'] &&
        config['headers']['Authorization'] == null) {
      return _setValidation(false, new http.Response(AuthError, 401));
    }

    if (!isValidMethod(method)) {
      return _setValidation(false, new http.Response(MethodError, 400));
    }

    return _setValidation(true, '');
  }

  _setValidation(bool isValid, dynamic response) {
    return {'isValid': isValid, 'response': response};
  }

  /// Checks for required config options with fallback base config options.
  _checkConfig(config) {
    if (config == null) {
      return this.baseConfig;
    } else {
      try {
        config['baseUrl'] =
            (config['baseUrl'] == null || config['baseUrl'].isEmpty)
                ? this.baseConfig['baseUrl']
                : config['baseUrl'];

        config['headers'] =
            (config['headers'] == null || config['headers'].isEmpty)
                ? this.baseConfig['headers']
                : config['headers'];
      } catch (e, st) {
        print(e);

        if (config['showStackTrace']) {
          print(st);
        }
      } finally {
        return {'baseUrl': config['baseUrl'], 'headers': config['headers']};
      }
    }
  }

  /// Format body to match the Content-Type header. Defaults to JSON.
  _formatBody(body, headers) {
    if (headers['Content-type'] == null || headers['Content-type'] == '') {
      return json.encode(body);
    }

    if (RegExp('(application/json)').hasMatch(headers['Content-type'])) {
      body = json.encode(body);
    }

    if (RegExp('(application/xml)').hasMatch(headers['Content-type']) ||
        RegExp('(text/xml)').hasMatch(headers['Content-type'])) {
      body = xml.parse(body['body']);
    }

    return body;
  }
}
