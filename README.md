# Bullseye

[![pipeline report](https://gitlab.com/tboccinfuso/bullseye/badges/master/pipeline.svg)](https://gitlab.com/tboccinfuso/bullseye/commits/master)

Bullseye is a HTTP request library built on [Dart](https://dart.dev/).

## Testing

From the root of the project: `pub run test`

_IF_ that is not working (git bash related): `dart tests/filename_test.dart`

<a target="_blank" href="https://icons8.com/icons/set/goal">Goal</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a>
