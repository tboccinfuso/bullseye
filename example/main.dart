import 'package:bullseye_request/bullseye.dart';
import 'package:http/http.dart';

const String testAPI = 'https://jsonplaceholder.typicode.com/';

Bullseye bullseye = Bullseye({
  'baseUrl': testAPI,
  'headers': {
    'Authorization': 'Bearer: fake-auth',
    'Content-Type': 'application/json'
  }
});

void main() async {
  Response response = await bullseye.get();

  print(response.body);
}
