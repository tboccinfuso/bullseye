import "package:test/test.dart";
import '../lib/tools/helpers.dart';

void main() {
  group('Helpers |', () {
    test('isUrl() regex', () {
      const String passingUrl = 'https://jsonplaceholder.typicode.com/';
      expect(isUrl(passingUrl), isTrue);

      const String failUrl = 'jsonplaceholder.typicode.com';
      expect(isUrl(failUrl), isFalse);
    });
  });
}
