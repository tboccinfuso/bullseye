import "package:test/test.dart";
import '../lib/bullseye.dart';

void main() async {
  group("Request |", () {
    test("var bullseye is type of Bullseye", () {
      const String testAPI = 'https://jsonplaceholder.typicode.com/';
      var bullseye = new Bullseye({
        'baseUrl': testAPI,
        'headers': {
          'Authorization': 'Bearer: asdas7d8as879dyhas798dahs7dha',
          'Content-Type': 'application/json'
        }
      });
      expect(bullseye, isA<Bullseye>());
    });

    test("Custom request made", () async {
      const String testAPI = 'https://jsonplaceholder.typicode.com/pages/1';
      var bullseye = new Bullseye({'baseUrl': testAPI, 'requiresAuth': true});

      var customRes = await bullseye.customRequest('get', {
        'url': 'https://jsonplaceholder.typicode.com/pages/2',
        'headers': {'Content-Type': 'application/json'}
      });
      expect(customRes.statusCode, greaterThanOrEqualTo(200));
    });

    test("get request made", () async {
      const String testAPI = 'https://jsonplaceholder.typicode.com/posts/1';
      var bullseye = new Bullseye({'baseUrl': testAPI, 'requiresAuth': false});

      var customRes = await bullseye.get();

      expect(customRes.statusCode, greaterThanOrEqualTo(200));
    });

    test("post request made", () async {
      const String testAPI = 'https://jsonplaceholder.typicode.com/posts/';
      var bullseye = new Bullseye({'baseUrl': testAPI, 'requiresAuth': false});

      var customRes = await bullseye.post({
        'title': 'foo',
        'body': 'bar',
        'userId': 1
      }, {
        'headers': {"Content-type": "application/json; charset=UTF-8"}
      });

      expect(customRes.statusCode, greaterThanOrEqualTo(200));
    });

    test("xml post request made", () async {
      const String testAPI =
          'https://api.beta.shipwire.com/exec/RateServices.php';
      var bullseye = new Bullseye({'baseUrl': testAPI, 'requiresAuth': false});

      var body =
          '''<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE RateRequest SYSTEM "http://www.shipwire.com/exec/download/RateRequest.dtd"><RateRequest><Username>api_user@example.com</Username><Password>yourpassword</Password><Order id="12579"><Warehouse>0</Warehouse><AddressInfo type="ship"><Address1>321 Foo bar lane</Address1><Address2>Apartment #2</Address2><City>Nowhere</City><State>CA</State><Country>US</Country><Zip>12345</Zip></AddressInfo><Item num="0"><Code>12345</Code><Quantity>1</Quantity></Item></Order></RateRequest>''';

      var customRes = await bullseye.post({
        'body': body,
      }, {
        'headers': {"Content-type": "application/xml;"}
      });

      expect(customRes.statusCode, greaterThanOrEqualTo(200));
    });

    test('inValid method on custom request', () async {
      const String testAPI = 'https://jsonplaceholder.typicode.com/pages/1';
      var bullseye = new Bullseye({'baseUrl': testAPI, 'requiresAuth': false});

      var customRes = await bullseye.customRequest('got', {
        'url': 'https://jsonplaceholder.typicode.com/pages/2',
        'headers': {'Content-Type': 'application/json'}
      });
      expect(customRes.statusCode, equals(400));
    });

    test('checkConfig falls back to default headers', () async {
      const String testAPI = 'https://jsonplaceholder.typicode.com/';
      var bullseye = new Bullseye({
        'baseUrl': testAPI,
        'requiresAuth': false,
        'headers': {
          'WWW-Authenticate': '',
          'Authorization': '',
          'Content-Type': '',
          'Accept': ''
        }
      });

      var customRes =
          await bullseye.customRequest('get', {'url': testAPI, 'headers': {}});

      expect(customRes.request.headers, equals(bullseye.baseConfig['headers']));
    });

    test("validateRequest to fail", () async {
      const String testAPI = 'https://jsonplaceholder.typicode.com/';
      var bullseye = new Bullseye({'baseUrl': testAPI, 'requiresAuth': true});

      var customRes = await bullseye.customRequest('POST', {
        'url': testAPI,
        'headers': {'Content-Type': 'application/json'}
      });
      expect(customRes.statusCode, equals(401));
    });
  });
}
