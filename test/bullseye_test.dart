import "package:test/test.dart";
import '../lib/bullseye.dart';

void main() {
  group('For Fun |', () {
    test('App == Awesome!', () {
      expect('Awesome!', equals('Awesome!'));
    });
  });

  group("new Bullseye() |", () {
    test("new Instance with custom config", () {
      const String testAPI = 'https://jsonplaceholder.typicode.com/';
      var bullseye = new Bullseye({
        'baseUrl': testAPI,
        'requiresAuth': true,
        'headers': {
          'WWW-Authenticate': '',
          'Authorization': '',
          'Content-Type': '',
          'Accept': ''
        }
      });

      expect(bullseye.baseConfig['requiresAuth'], equals(true));
    });

    test("Add 1 new config option & remove 1", () {
      const String testAPI = 'https://jsonplaceholder.typicode.com/';
      var bullseye = new Bullseye({
        'baseUrl': testAPI,
        'customConfigOption': true,
        'headers': {
          'WWW-Authenticate': '',
          'Authorization': '',
          'Content-Type': '',
          'Accept': ''
        }
      });

      expect(bullseye.baseConfig['customConfigOption'], equals(true));

      expect(bullseye.baseConfig['requiresAuth'], equals(null));
    });
  });

  group('Bullseye Class methods |', () {
    test('set_new_baseUrl()', () {
      const String testAPI = 'https://jsonplaceholder.typicode.com/';
      const String newBaseUrl = 'http://something.com';
      Bullseye bullseye = new Bullseye({
        'baseUrl': testAPI,
        'requiresAuth': false,
        'headers': {
          'WWW-Authenticate': '',
          'Authorization': '',
          'Content-Type': '',
          'Accept': ''
        }
      });

      bullseye.set_new_baseUrl(newBaseUrl);

      expect(bullseye.baseConfig['baseUrl'], equals(newBaseUrl));
    });

    test("update_base_headers()", () {
      const String testAPI = 'https://jsonplaceholder.typicode.com/';
      final newHeaders = {'Token': '123'};

      var bullseye = new Bullseye({
        'baseUrl': testAPI,
        'requiresAuth': true,
        'headers': {
          'WWW-Authenticate': '',
          'Authorization': '',
          'Content-Type': '',
          'Accept': ''
        }
      });

      bullseye.update_base_headers(newHeaders);

      expect(bullseye.baseConfig['headers']['Token'], equals('123'));
    });

    test("get_base_config()", () {
      const String testAPI = 'https://jsonplaceholder.typicode.com/';
      final config = {
        'baseUrl': testAPI,
        'requiresAuth': true,
        'headers': {
          'WWW-Authenticate': '',
          'Authorization': '',
          'Content-Type': '',
          'Accept': ''
        }
      };

      var bullseye = new Bullseye(config);

      expect(bullseye.get_base_config(), equals(config));
    });
  });
}
